// A compiler avec : gcc CHD.c -g3 -Wextra -pedantic -ansi -std=c99 -W -Wall -L /usr/X11R6/lib -lX11 -lm -o CHD

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

/* ---- Global ---- */
int	   	scr;
unsigned int	DIMw, DIMh, DIMp;
short int decimal[3];
char hexa[3][3];

XFontStruct	*policeStruct;
Display		*dsp;
Window		fen;
XEvent		ev;
GC		cg;

/* ---- Prototypage ---- */
void openWindow(void);
void closeWindow(void);

unsigned long int couleurDef(short int, short int, short int);
void hexToDec(char [7]);

void encre(unsigned long int);
void fond(unsigned long int);
void rectangleFull(int, int, int, int);
void ligne(int, int, int, int);

void textPrint(int, int, const char *);
void questionsDH(void);
void eventManager(void);

/* ---- Main ---- */
int main(void)
{
    int dimWUsed, dimHUsed;
    char back[15];
    
    questionsDH();
    openWindow();
    
    // Un petit nom
    XStoreName(dsp, fen, "Convertisseur de couleur RVB HexDec | (q) pour quiter");
    printf("Exa [RRVVBB] %s%s%s\n", hexa[0], hexa[1], hexa[2]);
    printf("Exa R: %s V: %s B: %s\n", hexa[0], hexa[1], hexa[2]);
    printf("Dec R: %hd V: %hd B: %hd\n", decimal[0], decimal[1], decimal[2]);
    
    // On charge la couleur
    fond(couleurDef(decimal[0], decimal[1], decimal[2]));
    
    // On afiche les infos
    dimWUsed = DIMw / 12;
    dimHUsed = DIMh / 12;
    
    encre(couleurDef(0, 0, 0));
    rectangleFull(dimWUsed * 3, dimHUsed * 3, dimWUsed * 6, dimHUsed * 6);
    textPrint(dimWUsed * 3 + 10, dimHUsed * 3 - 12, "Convertisseur RVB, Hexa <=> Deci");
    textPrint(dimWUsed * 3 + 10, dimHUsed * 9 + 15, "Quiter (q)");
    
    printf("DIM 1: %d 2: %d | USED 11: %d 22: %d | Centre t: %d tt: %d\n", DIMw, DIMh, dimWUsed, dimHUsed, dimWUsed * 6, dimHUsed * 6);
    
    encre(couleurDef(decimal[0], decimal[1], decimal[2]));
    
    ligne((dimWUsed * 3) + (dimWUsed * 2), dimHUsed * 3 + 10, (dimWUsed * 3) + (dimWUsed * 2), dimHUsed * 9 - 10);
    ligne((dimWUsed * 3) + (dimWUsed * 4), dimHUsed * 3 + 10, (dimWUsed * 3) + (dimWUsed * 4), dimHUsed * 9 - 10);
    
    ligne(dimWUsed * 3 + 10, (dimHUsed * 3) + (dimHUsed * 1.5), dimWUsed * 9 - 10, (dimHUsed * 3) + (dimHUsed * 1.5));
    ligne(dimWUsed * 3 + 10, (dimHUsed * 3) + (dimHUsed * 3), dimWUsed * 9 - 10, (dimHUsed * 3) + (dimHUsed * 3));
    ligne(dimWUsed * 3 + 10, (dimHUsed * 3) + (dimHUsed * 4.5), dimWUsed * 9 - 10, (dimHUsed * 3) + (dimHUsed * 4.5));
    
    encre(couleurDef(255, 0, 0));
    textPrint(dimWUsed * 3 + dimWUsed / 2, dimHUsed * 3 + dimWUsed / 2, "R");
    textPrint(dimWUsed * 3 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 1.5 + dimWUsed / 2, "Rouge");
    snprintf(back, 3, "%s", hexa[0]);
    textPrint(dimWUsed * 3 + dimWUsed * 2 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 1.5 + dimWUsed / 2, back);
    snprintf(back, 4, "%d", decimal[0]);
    textPrint(dimWUsed * 3 + dimWUsed * 4 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 1.5 + dimWUsed / 2, back);
    
    encre(couleurDef(0, 255, 0));
    textPrint(dimWUsed * 3 + dimWUsed / 2 + 7, dimHUsed * 3 + dimWUsed / 2, "V");
    textPrint(dimWUsed * 3 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 3 + dimWUsed / 2, "Vert");
    snprintf(back, 3, "%s", hexa[1]);
    textPrint(dimWUsed * 3 + dimWUsed * 2 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 3 + dimWUsed / 2, back);
    snprintf(back, 4, "%d", decimal[1]);
    textPrint(dimWUsed * 3 + dimWUsed * 4 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 3 + dimWUsed / 2, back);
    
    encre(couleurDef(0, 50, 255));
    textPrint(dimWUsed * 3 + dimWUsed / 2 + 14, dimHUsed * 3 + dimWUsed / 2, "B");
    textPrint(dimWUsed * 3 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 4.5 + dimWUsed / 2, "Bleu");
    snprintf(back, 3, "%s", hexa[2]);
    textPrint(dimWUsed * 3 + dimWUsed * 2 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 4.5 + dimWUsed / 2, back);
    snprintf(back, 4, "%d", decimal[2]);
    textPrint(dimWUsed * 3 + dimWUsed * 4 + dimWUsed / 2, dimHUsed * 3 + dimHUsed * 4.5 + dimWUsed / 2, back);
    
    encre(couleurDef(255, 255, 255));
    snprintf(back, 15, "Hexa [#%s%s%s]", hexa[0], hexa[1], hexa[2]);
    textPrint(dimWUsed * 3 + dimWUsed * 2 + dimWUsed / 2, dimHUsed * 3 + dimWUsed / 2, back);
    textPrint(dimWUsed * 3 + dimWUsed * 4 + dimWUsed / 2, dimHUsed * 3 + dimWUsed / 2, "Decimal");
    
    eventManager();

    return 0;
}

/* ---- Fonctions ---- */

/* --- Fenetre --- */
void openWindow(void)
{
    char *fontName;
    
    unsigned int	i;
    unsigned long	cw;
    
    XGCValues			vg;
    XSetWindowAttributes	att;
    
    if((dsp = XOpenDisplay(NULL)) == NULL)
    {
	fprintf(stderr, "Impossible d'accéder au périphérique d'affichage %s.\n", getenv("DISPLAY"));
	exit(-1);
    }
    
    scr = DefaultScreen(dsp);
    DIMw = XDisplayWidth(dsp, scr);
    DIMh = XDisplayHeight(dsp, scr);
    DIMp = XDisplayPlanes(dsp, scr);
    
    if((DIMw < DIMh) && (DIMw != DIMh))
    {
	DIMh = DIMh / 4;
	
	if((i = DIMh * 2) <= DIMw)
	{
	    DIMw = DIMh * 2;
	}
    }
    else
    {
	DIMw = DIMw / 2;
	DIMh = DIMh / 2;
    }
    
    cw = CWBackPixel | CWBorderPixel;
    att.border_pixel = WhitePixel(dsp, scr);
    att.background_pixel = BlackPixel(dsp, scr);
    
    fen = XCreateWindow(dsp, RootWindow(dsp, scr), 0, 0,
    DIMw, DIMh, 4, DefaultDepth(dsp, scr), InputOutput, NULL, cw, &att);
    
    XSelectInput(dsp, fen, KeyReleaseMask | ExposureMask);
    
    cg = XCreateGC(dsp, fen, 0, &vg);
    
    fontName = "9x15";
    if((policeStruct = XLoadQueryFont(dsp, fontName)) == NULL)
    {
	fprintf(stderr, "Police de caractères %s non trouvée \n", fontName);
	exit (-1);
    }
    
    XSynchronize (dsp, True);
    XSync (dsp, False) ;
    
    XMapWindow(dsp, fen);
    XMaskEvent(dsp, ExposureMask, &ev);
}

void closeWindow(void)
{    
    XSynchronize(dsp, False);
    XFreeFont(dsp, policeStruct);
    XFreeGC(dsp, cg);
    XDestroyWindow(dsp, fen);
    XCloseDisplay(dsp);
   
    printf("A+ mon pote !\n");
}

/* --- Couleurs --- */
unsigned long int couleurDef(short int r, short int v, short int b)
{
    return (r << 16 | v << 8 | b);
}

void hexToDec(char codeHex[7])
{    
    char i[5];
    
    hexa[0][0] = codeHex[0];
    hexa[0][1] = codeHex[1];
    hexa[0][2] = '\0';
    
    hexa[1][0] = codeHex[2];
    hexa[1][1] = codeHex[3];
    hexa[1][2] = '\0';
    
    hexa[2][0] = codeHex[4];
    hexa[2][1] = codeHex[5];
    hexa[2][2] = '\0';
    
    snprintf(i, sizeof(i), "0x%s", hexa[0]);
    decimal[0] = (short int)strtol(i, NULL, 0);
    
    snprintf(i, sizeof(i), "0x%s", hexa[1]);
    decimal[1] = (short int)strtol(i, NULL, 0);
    
    snprintf(i, sizeof(i), "0x%s", hexa[2]);
    decimal[2] = (short int)strtol(i, NULL, 0);
}

/* --- Decos/Formes --- */
void encre(unsigned long int col)
{// Changer la couleur d'écriture
    XSetForeground(dsp, cg, col);
}

void fond(unsigned long int col)
{ // On efface la fenêtre avec une nouvelle couleur
    XSetWindowBackground(dsp, fen, col);
    XClearWindow(dsp, fen);
}

void rectangleFull(int x, int y, int w, int h)
{
    XFillRectangle(dsp, fen, cg, x, y, w, h);
}

void ligne(int x, int y, int x2, int y2)
{
    XDrawLine(dsp, fen, cg, x, y, x2, y2);
}

/* --- Actions --- */
void textPrint(int x, int y, const char *string)
{
    XDrawString(dsp, fen, cg, x, y, string, strlen(string));
}

void questionsDH(void)
{
    char dorH, codeHex[7];
    short int decR, decV, decB;
    
    printf("Que voulez vous utiliser (d)écimals ou (c)odeHexa [D/c] (\"q\" pour quiter) :\n  ==> ");
    dorH = fgetc(stdin);
    
    switch(dorH) {
	case 81 :
	case 113 :
	    exit(1);
	    break;
	case 67 :
	case 99 :
	    printf("Entrer votre CodeHexa [RRVVBB] :\n  ==> ");
	    scanf("%s", codeHex);
	    
	    hexToDec(codeHex);
	    break;
	case 68 :
	case 100 : 
	default :
	    printf("Entrer votre Code Décimal [0-255] :\n");
	    printf("R ==> ");
	    scanf("%hd", &decR);
	    printf("V ==> ");
	    scanf("%hd", &decV);
	    printf("B ==> ");
	    scanf("%hd", &decB);
	    
	    decimal[0] = decR;
	    decimal[1] = decV;
	    decimal[2] = decB;
	    
	    snprintf(hexa[0], sizeof(hexa[0]), "%x", decimal[0]);
	    snprintf(hexa[1], sizeof(hexa[1]), "%x", decimal[1]);
	    snprintf(hexa[2], sizeof(hexa[2]), "%x", decimal[2]);
	    break;
    }
}

void eventManager(void)
{
    int close = 0;
    
    while(!close)
    {
	XNextEvent(dsp, &ev);

	if (ev.type == KeyRelease)
	{
	    char buffer;
	    int bufsize=1;
	    
	    XLookupString(&(ev.xkey), &buffer, bufsize, NULL, NULL);
	    
	    switch(buffer) {
		case 'q':
		    close = 1;
		    closeWindow();
		    break;
		case 'p':
		    break;
		case 'm':
		    break;
	    }
	}
    }
}